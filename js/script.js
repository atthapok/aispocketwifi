var customerProfile = {}
customerProfile.status = null;  //----- MAMYPOKO
customerProfile.nomsisdn = null;
customerProfile.msisdn = null;
customerProfile.businessType = null;

customerProfile.usage = {};
customerProfile.usage.isUsage = false;
customerProfile.usage.internet = null;			// gb mb kb
customerProfile.usage.condition = null;			//  คุณสามารถใช้เน็ตได้ไม่จำกัด ด้วยความเร็วตามรายละเอียดแพ็คเกจของคุณ  if null not show
customerProfile.usage.balance = {};				//  
customerProfile.usage.balance.bath = null;		//  เงินคงเหลือ บาท
customerProfile.usage.balance.stang = null;	//  เงินคงเหลือ สตางค์
customerProfile.usage.time = null;					//  สิ้นสุด
customerProfile.usage.timeAmount = null;		//  เวลาที่ใช้ไป

var interval = null;  //----- MAMYPOKO
var flagcheck = false;

function getDataFromeService(url, async, callback) {
    //console.log("API: " + url)
    var request = $.ajax({
        url: url,
        type: "GET",
        cache: false,
        async: async
    });

    request.done(function (msg) {
        //console.log(JSON.stringify(msg, null, 2));
        if (callback) {
            callback(msg);
        }
    });

    return request;
}

function navigateTonavigateToBrowser(url) {

    var iPadAgent = navigator.userAgent.match(/iPad/i) != null;
    var iPodAgent = navigator.userAgent.match(/iPod/i) != null;
    var iPhoneAgent = navigator.userAgent.match(/iPhone/i) != null;
    var AndroidAgent = navigator.userAgent.match(/Android/i) != null;
    var webOSAgent = navigator.userAgent.match(/webOS/i) != null;
    var safariAgent = navigator.userAgent.match(/Safari/i) != null;

    if (iPadAgent || iPodAgent || iPhoneAgent) {
        var a = document.createElement('a');
        a.href = url;
        a.target = "_blank";
        a.click();
    } else if (AndroidAgent) {
        navigator.app.loadUrl(url, { openExternal: true });
    } else {
        window.location.href = url;
    }
}

function getCustomerProfile() {

    // step 1 check msisdn
    getDataFromeService(APIeService.chkMSISDN, false, function (responseCheckMSISDN) {
		customerProfile.status = responseCheckMSISDN.status;  //----- MAMYPOKO
        
		// msisdn null
		if (responseCheckMSISDN == false || responseCheckMSISDN.status != "000") {
            console.log("ERROR: Request CheckMSISDN Error");
        } 
		
		else {
			// msisdn null
            if (responseCheckMSISDN.msisdn == "!MSISDN!" || responseCheckMSISDN.msisdn == undefined) {
                console.log("ERROR: CheckMSISDN !MSISDN! Error");
            } 
			// msisdn not null
			else {
                customerProfile.msisdn = "0" + responseCheckMSISDN.msisdn.substring(2, responseCheckMSISDN.msisdn.length);

                // step 2 get OTP
                var parameter = "&mobileNo=" + customerProfile.msisdn + "&bypass=true&key=" + responseCheckMSISDN.key;
                getDataFromeService(APIeService.getOTP + parameter, true, function (responseOTP) {
                    if (responseOTP == false || responseOTP.status != "000") {
                        customerProfile.status = "999";
						console.log("ERROR: Request OTPBypass Error");
                    } else {
						// step 3 get Profile and Usage
						
                        customerProfile.businessType = responseOTP.callOTPResponse.businessType;
						$('#loadingProfile').show();
						
						// check business type cutomer
                        if (customerProfile.businessType.toLowerCase().indexOf("postpaid") != -1) {

                            // ======================== Postpaid Code here ========================

							// Get CQL
                            getDataFromeService(APIeService.cqs, true, function (responseCQS) {
                                // get cql fail
								if (responseCQS == false || responseCQS.status != "000") {
									customerProfile.status = "999";
                                    console.log("ERROR: Request CQS Error");
                                } else {
                                    // check internet
                                    if ((responseCQS.dataTextThai.match("ยอดรวม Internet คงเหลือที่สามารถใช้งานได้ คือ") == null) || responseCQS.dataTextThai == null || responseCQS.dataTextThai == "") {
                                        customerProfile.usage.isUsage = false;
                                        customerProfile.usage.internet = responseCQS.dataTextThai;
                                    } else {
                                        customerProfile.usage.isUsage = true;
                                        customerProfile.usage.internet = responseCQS.dataTextThai.replace("ยอดรวม Internet คงเหลือที่สามารถใช้งานได้ คือ", "");
                                    }

                                    // check condition
                                    if (responseCQS.conditionThai != null && responseCQS.conditionThai != "") {
                                        customerProfile.usage.condition = responseCQS.conditionThai;
                                    }
									
									// show usage postpaid
									$('#loadingProfile').hide();
									$('#postpaid').show();
									showUsage();
                                }

                            });

						}else {

                            // ======================== Prepaid Code here ========================

							// Get Usage
							getDataFromeService(APIeService.usage, true, function (responseUsage) {
								// get usage fail
                                if (responseUsage == false || responseUsage.status != "000") {
                                    customerProfile.status = "999";
									console.log("ERROR: Request Usage Error");
									
                                } else {

                                    // check balance
                                    if (responseUsage.usage.remainBalance == "" || responseUsage.usage.remainBalance == null) {
                                        customerProfile.usage.balance = 0;
                                    } else {
                                        var tempBalance = responseUsage.usage.remainBalance.split(".");
                                        customerProfile.usage.balance.bath = tempBalance[0];
                                        customerProfile.usage.balance.stang = tempBalance[1];
                                    }

                                    // check TimeAmount
                                    if (responseUsage.usage.remainTimeAmount == "" || responseUsage.usage.remainTimeAmount == null) {
                                        customerProfile.usage.timeAmount = 0;
                                    } else {
                                        customerProfile.usage.timeAmount = responseUsage.usage.remainTimeAmount;
                                    }

                                    customerProfile.usage.time = genDateThai(responseUsage.usage.remainTimeThai);
									
									
									// Get CheckPromo for disply internet usage
									getDataFromeService(APIeService.checkPromo, true, function (responseCheckPromo) {
										// get checkpromo fail
										if (responseCheckPromo == false || responseCheckPromo.status != "000") {
											customerProfile.status = "999";
											console.log("ERROR: Request CheckPromo Error");
										} else {
											
											
											if ((responseCheckPromo.CHKPROMO == null || responseCheckPromo.CHKPROMO == "") || responseCheckPromo.CHKPROMO.promotion.length == 0) {
												// usage null go to mypromo api
												getMyPromotion();
											} else {
												// usage not null
												var promo = $.grep(responseCheckPromo.CHKPROMO.promotion, function (e) { return e.type == "แพ็กเกจ Internet"; });

												if (promo == "" || promo == null) {
													// usage null go to mypromo api				
													getMyPromotion();
												} else {
													// usage not null				
													if (customerProfile.businessType.toLowerCase().indexOf("3g") != -1) {

														// ======================== 3G Code here ========================

														if (promo[0].reward.length == 0) {
															getMyPromotion();
														} else {
															customerProfile.usage.isUsage = true;
															customerProfile.usage.internet = promo[0].reward[0].replace("ยอดรวม Internet คงเหลือที่สามารถใช้งานได้ คือ", "");
															if (promo[0].reward.length > 1) {
																customerProfile.usage.condition = "";
																for (var i = 1; i < promo[0].reward.length; i++) {
																	customerProfile.usage.condition += " " + promo[0].reward[i];
																}
															}
														}
													}else {

													// ======================== 2G Code here ========================
														if (promo[0].promos.length > 0) {
															customerProfile.usage.isUsage = true;
															var tempUsage = promo[0].promos[0].remain.split("<br>");
															customerProfile.usage.internet = tempUsage[0].replace("Internet คงเหลือ","");
															customerProfile.usage.condition = tempUsage[1];
														}

													}

													// show usage prepaid
													$('#loadingProfile').hide();
													$('#prepaid').show();
													showUsage();
												}

											}
										} 
                                            
									});
                                    
                                }

								
                            });

							
                        }
					}
                });
            }
        }
	//}
    });
}

function getConnectPocket() {
	
	// show connecting
	$('#connecting').show();
	do_connecting();

	// check status pocket wifi
    var request = getDataFromeService(settingPocketWifi, false);
	//console.log(JSON.stringify(request, null, 2))
	//request.status = 200;

	// check not connect pocket wifi
	if (request.status != "200") {  // DEFAULT is 200
		request = null;
		request = getDataFromeService(settingPocketWifi+"login.asp", false);
		
		// check not connect pocket wifi
		if (request.status != "200") {
			
			$('#pleaseconnect').show();  //----- MAMYPOKO
			//do_refresh();
			$('*#refresh img').addClass('img-refresh');
			setTimeout(function() {
				flagcheck = true;
				getConnectPocket();
			}, 2000);

			console.log("กรุณาเชื่อมต่อ AIS Pocket Wifi");
		}else{
			// connect wifi
			if (flagcheck)
			{
				window.location.reload();
			}
			afterCheckPocketWifi();
		}

    } else {
		// connect wifi
		if (flagcheck)
			{
				window.location.reload();
			}
		afterCheckPocketWifi();
    }
}

function afterCheckPocketWifi(){
	// get customer profile from eService
	getCustomerProfile();
		
		//----- START MAMYPOKO
		// Mobile number is not null 
		if(customerProfile.status == "000") {
			
			// Show animate connect wifi 5 sec befor to home page
			setTimeout(function() {
				$('#connecting').hide();
				$('#pleaseconnect').hide();
				$('#connected').show();
				$('#package').show();
				do_connected('package');
				clearInterval(interval);
			}, 5000);
			
		} 
		// Mobile number is null 
		else {
			// Set Cutomer Unknow
			customerProfile.nomsisdn = true;
			
			// Show animate connect wifi 5 sec befor to home page
			setTimeout(function() {
				$('#connecting').hide();
				$('#pleaseconnect').hide();
				$('#connected').show();
				$('#unknown').show();
				do_connected('unknown');
				clearInterval(interval);
			}, 5000);

		}
		//----- END MAMYPOKO
}

function getMyPromotion() {
    getDataFromeService(APIeService.myPromo, true, function (responsemyPromo) {
        
		if (responsemyPromo == false || responsemyPromo.status != "000") {
			customerProfile.status = "999";
			console.log("ERROR: Request myPromo Error");
        } else {
            if (responsemyPromo.myprofile.internetRemainTH != null && responsemyPromo.myprofile.internetRemainTH != "") {
				customerProfile.usage.isUsage = true;
                customerProfile.usage.condition = "";
                if (responsemyPromo.myprofile.internetRemainTH.match("ยอดรวม Internet คงเหลือที่สามารถใช้งานได้ คือ") != null) {
					responsemyPromo.myprofile.internetRemainTH = responsemyPromo.myprofile.internetRemainTH.replace("ยอดรวม Internet คงเหลือที่สามารถใช้งานได้ คือ ", "");
                    customerProfile.usage.internet = responsemyPromo.myprofile.internetRemainTH.substring(0, responsemyPromo.myprofile.internetRemainTH.indexOf('KB') + 2);
                    customerProfile.usage.condition = responsemyPromo.myprofile.internetRemainTH.substring(responsemyPromo.myprofile.internetRemainTH.indexOf('KB') + 2, responsemyPromo.myprofile.internetRemainTH.length);
				}else{
					customerProfile.usage.internet = responsemyPromo.myprofile.internetRemainTH;
				}

				// show usage prepaid
				$('#loadingProfile').hide();
				$('#prepaid').show();
				showUsage();
            } else {

                customerProfile.usage.isUsage = false;
                customerProfile.usage.internet = responsemyPromo.myprofile.internetRemainTH;
				customerProfile.status = "999";
				
				// show usage prepaid
				$('#loadingProfile').hide();
				$('#prepaid').show();
				showUsage();
            }
        }
        
    });
}

function genDateThai(date) {
    var monthNames = ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน",
    "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"];

    var tempDate = date.split("/");
	
	if (tempDate[1].length == 2)
	{
		if (tempDate[1].substr(0,1) == 0)
		{
			tempDate[1] = tempDate[1].substr(1,1);
		}
	}

	tempDate[1] = tempDate[1]-1;

    return tempDate[0] + " " + monthNames[tempDate[1]] + " " + tempDate[2];
}


//----- START MAMYPOKO


function do_connecting() {
	// if #connecting show()
	if($('#connecting').is(':visible')) {
		$i = 1;
		interval = setInterval(function() {
			if($i <= 3) {
				$('#pic').attr('src', '../img/loadingpage/iconB_PocketWifi0' + $i + '@2x.png');
				$i++;
			}
			else {
				$i = 1;
				$('#pic').attr('src', '../img/loadingpage/iconB_PocketWifi04@2x.png');
			}
		}, 1000);
	}
}

function do_connected(process) {
    if(process == 'package') {
		// if #connected and #package show()
		if($('#connected').is(':visible') && $('#package').is(':visible')) {
			$("#msisdn").text("หมายเลข " + customerProfile.msisdn);
			$("#usage").html(customerProfile.usage);
		}
	} else if(process == 'unknown') {
		// if #connected and #unknown show()
		if($('#connected').is(':visible') && $('#unknown').is(':visible')) {
			$("#msisdn").text("ไม่สามารถค้นหาเลขหมายได้");
			//do_refresh();
		}
	} else {
		return false;
	}
}

function checkVersion(){
	getDataFromeService(urlCheckVersion, false, function(msg){
		if (msg != null)
		{
			//console.log(JSON.stringify(msg, null, 2));
			if (msg.onStore == "T")
			{

				if (!alert("กรุณาอัพเดท Version บน Apple Store"))
				{
					navigateTonavigateToBrowser("http://www.ais.co.th/pocketwifiapp/");
					navigator.app.exitApp();

				}
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	});
}

function showUsage(){
if (customerProfile.usage.internet == null)
	{
		customerProfile.usage.internet = "0KB";
	}
				
	$('.usage-internet').text("Internet คงเหลือ " + customerProfile.usage.internet);
				

	if (customerProfile.usage.condition == null || customerProfile.usage.condition == '')
	{
		$('.ul_usage_condition').hide();
	}else{
		$('.ul_usage_condition').show();
	}
				
	$('.usage-condition').text(customerProfile.usage.condition);

	$('#usage-balance-bath').text(customerProfile.usage.balance.bath + " บาท");
	$('#usage-balance-stang').text(customerProfile.usage.balance.stang + " สตางค์");
	$('#usage-time').text(customerProfile.usage.timeAmount + " วัน ใช้งานได้ถึง " + customerProfile.usage.time);
}